const button = document.querySelector("button");

const SpeechRecognition = 
  window.SpeechRecognition || window.webkitSpeechRecognition;

const recognition = new SpeechRecognition();

recognition.onstart = function () {
    console.log("Speech Recognition started!");
};

recognition.onresult = function (event) {
    console.log(event);

    const spokenwords = event.results[0][0].transcript;

    console.log("spoken words are",spokenwords);
    computerSpeech(spokenwords);
};

function computerSpeech(words) {
    const speech = new SpeechSynthesisUtterance();
    speech.lang = "en-US"; 
    speech.pitch = 0.9;
    speech.volume = 1;
    speech.rate = 1;

    determinewords(speech,words);

    window.speechSynthesis.speak(speech);
} 


   function determinewords(speech,words){
    if(words.includes("How are you")){
        speech.text = "I am fine, thank you!";
    }

    if(words.includes("Hello")) {
        speech.text = "hi sir, how can i help you";
    }

    if(words.includes("Open Facebook")) {
        speech.text = "opening facebook";
        window.open("https://www.facebook.com/");
    }

    
    if(words.includes("Open Google")) {
        speech.text = "opening Google";
        window.open("https://www.google.co.in/"); 
    }

    if(words.includes("Open Twitter")) {
        speech.text = "opening Twitter";
        window.open("https://twitter.com/?lang=en-in");
    }

    if(words.includes("Open Instagram")) {
        speech.text = "opening Instagram";
        window.open("https://www.instagram.com/");
    }

    if(words.includes("Open Gmail")) { 
        speech.text = "opening gmail";
        window.open("https://mail.google.com");
    }

    
    if(words.includes("Open Wikipedia")) {
        speech.text = "opening wikipedia, hold on";
        window.open("https://www.wikipedia.org/")
    }
    
    if(words.includes("Open Lords website")) {
        speech.text = "opening lords website, hold on";
        window.open("https://www.lords.ac.in/")
    }
    
    if(words.includes("Open geeks for geeks")) {
        speech.text = "opening geeks for geeks, hold on";
        window.open("https://www.geeksforgeeks.org/")
    }
    
    

    if(words.includes("Do you love me")) {
        speech.text = "why should I love you?";
    }
    
    }
 
   
button.addEventListener("click", ()  => {
    recognition.start();
 });
